#ifndef _log_h_
#define _log_h_

#include "time.h"

#define LOGBUF 4
#define ROWSIZE 1024
typedef struct {
  time_t time;
  char text[1024];
} logentry;

logentry logs[LOGBUF];

void postLogEntry(const char *msg);

#endif
