#include "log.h"
#include "string.h"

void postLogEntry(const char *msg){
  int i;
  for(i = 1; i < LOGBUF; i++)
    logs[i - 1] = logs[i];
  strcpy(logs[LOGBUF-1].text, msg);
  time(&logs[LOGBUF-1].time);
}
