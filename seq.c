#include "common.h"
#include "seq.h"
#include "pattern.h"

void seq_step(seq *s,int speed){
	int notef;
	unsigned char c;
	if(speed < 1)
		speed = 1;
	if(s->playing){
		if( s->first){
			s->lastNote = 0;
			s->first = 0;
		} else
			s->lastNote = patterns[s->pattern].data[s->step];
		s->slide = GETSLIDE(s->lastNote);
		s->step++;
		if(s->step >= patterns[s->pattern].length){
			s->step = 0;
			s->pattern = s->nextPattern;
		}
		c = patterns[s->pattern].data[s->step];
		notef = notes[GETNOTE(c)+(s->transpose-0x59)+12*GETOCTUP(c)-12*GETOCTDOWN(c)];
		if(GETNOTE(c)<=0xc){
			if(!s->slide)
				voice_trig(&(s->v),notef, notef, speed, 1);
			else
				voice_trig(&(s->v), s->lastFreq, notef, speed, 0);
		}
		s->slide = GETSLIDE(c);
		s->lastFreq = notef;
	}
	
}

void seq_halfStep(seq *s){
	if(!s->slide)
		s->v.gate = 0;
}

void seq_stop(seq *s){
	s->playing = 0;
	s->step = 0;
	s->v.gate = 0;
	playState = 0;
}
void seq_start(seq *s){
	if(!s->playing){
		s->first = 1;
		s->playing = 1;
		s->step = patterns[s->pattern].length;
		seq_step(s,tempoSteps[0]);
		tick = 0;
		flip = 1;
		playState = 1;
		lastTickStep = -1;
	}
}
