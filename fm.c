#include "common.h"
#include "fm.h"

#define AMP(en) (((((en).ampState)>>2) * (en).amp)>>8)

void voice_setFreq(voice *v, int frequency) {
	v->op1.phaseInc = frequency;
	v->op2.phaseInc = frequency;
}

void voice_trig(voice *v, int frequency, int target, int speed,unsigned char retrig) {
	if(retrig){
		v->op1.phase = 0;
		v->op2.phase = 0;
		v->en1.ampState = 0x3ffff;
		v->en2.ampState = 0x3ffff;
	}
	v->gate = 1;
	v->op1.phaseInc = frequency;
	v->op2.phaseInc = frequency;
	v->target = target;
	v->freqIncrement = (target - frequency) / speed;
}

int voice_tick(voice *v) {
	int s;
	
	// Calculate op1 * en1
	s = (((sine[((v->op1.phase * (1 + v->op1Mul) >> 20) + ((v->op1Last * v->fb) >> 10)) & 0xff] >> 4) * AMP(v->en1)) >> 16);
	
	// Save as last sample of op1 * en1
	v->op1Last = s;
	
	// Calculate op2 * en2
	s = (sine[ ( (v->op2.phase*(1 + v->op2Mul) >> 20) + s) & 0xff ] * AMP(v->en2))>>16;
	
	
	// Update envelopes
	if(v->gate) {
		v->en1.ampState -= ((((0xff - v->en1.decay) * ratio)>>8) * v->en1.ampState)>>18;
		v->en2.ampState -= ((((0xff - v->en2.decay) * ratio)>>8) * v->en2.ampState)>>18;
		if(v->en1.ampState < (v->en1.sustain<<10))
			v->en1.ampState = (v->en1.sustain<<10);
		if(v->en2.ampState < (v->en2.sustain<<10))
			v->en2.ampState = (v->en2.sustain<<10);
	} else {
		v->en1.ampState -= ((((0xff * v->en1.ampState)>>18) * ratio)>>8)+1;
		v->en2.ampState -= ((((0xff * v->en2.ampState)>>18) * ratio)>>8)+1;
	}
	
	// Update operators
	v->op1.phase += v->op1.phaseInc;
	v->op2.phase += v->op2.phaseInc;
	
	// Slide
	v->op1.phaseInc += v->freqIncrement;
	v->op2.phaseInc += v->freqIncrement;
	if(v->freqIncrement > 0 && v->op1.phaseInc > v->target){
		v->op1.phaseInc = v->target;
		v->op2.phaseInc = v->target;
	} else if(v->freqIncrement < 0 && v->op1.phaseInc < v->target) {
		v->op1.phaseInc = v->target;
		v->op2.phaseInc = v->target;
	}
		
	
	return s;
}
