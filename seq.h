#ifndef _seq_h_
#define _seq_h_

#include "fm.h"

typedef struct {
	char step;
	unsigned char pattern;
	unsigned char nextPattern;
	unsigned char playing;
	unsigned char slide;
	int lastFreq;
	unsigned char baseNote;
	voice v;
	unsigned char transpose;
	unsigned char lastNote;
	unsigned char first;
} seq;

void seq_step(seq *s, int speed);
void seq_stop(seq *s);
void seq_start(seq *s);
void seq_halfStep(seq *s);
int flip;
int playState;
int last_playState;
int tickStep;
int lastTickStep;

#endif
