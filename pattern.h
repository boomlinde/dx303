#ifndef _pattern_h_
#define _pattern_h_

#define SETSLIDE(s,n)
#define SETOCTUP(s,n)
#define SETOCTDOWN(s,n)
#define SETNOTE(s) ((s)=((s)&0x0f)|((s)<<4))

#define GETSLIDE(s) ((s)&0x2)
#define GETOCTUP(s) (((s)&0x4)>>2)
#define GETOCTDOWN(s) (((s)&0x8)>>3) 
#define GETNOTE(s) (((s)&0xf0)>>4)

typedef struct {
	unsigned char data[32];
	char length;
	
} pattern;

#endif
