#include "common.h"
#include "fm.h"
#include "stdlib.h"
#include "tempo.h"
#include "fileio.h"
#include "log.h"
#include "string.h"
#include "time.h"
#include "ncurses.h"

#define HON(s,n,p)attrset(COLOR_PAIR(2));if(selected==(n)){attrset(A_REVERSE|COLOR_PAIR(3));temp = (p) + temp;if( temp < 0) temp = 0;if( temp > 0xff ) temp = 0xff;(p) = temp;}printw("%s[%02X]",(s),(p));attrset(A_NORMAL);

#define INC(v,n){temp=(v)+(n);if(temp>0xff)temp=0xff;if(temp<0)temp=0;(v)=temp;}
#define RIGHT 261
#define LEFT 260
#define UP 259
#define DOWN 258

void drawParams(int x, int y, seq *s, int c);
void drawSequencer(int x, int y, seq *s);
void drawSeqRow(int x, int y, unsigned char n, char bold);
void printLogs(int x, int y);

static int selected = 0;
short int temp = 0;
static seq *current = &a;
unsigned char *noteNames[17] = {"SL","DN","UP","--"," C","#C"," D","#D"," E"," F","#F"," G","#G"," A","#A"," B"," C"};
static unsigned char tempNote;
static unsigned char seqToggle = 0;
static int oldSelected = 0;
static int seqStep = 0;
static unsigned char playToggle = 0;

void startInterface(){
	int c = 0;
	int i;
	initscr();
	if(has_colors()){
		start_color();
		init_pair(1, COLOR_YELLOW,COLOR_MAGENTA);
		init_pair(2, COLOR_GREEN,COLOR_BLACK);
		init_pair(3, COLOR_YELLOW,COLOR_BLACK);
    init_pair(4, COLOR_WHITE,COLOR_BLACK);
	}
	noecho();
	keypad(stdscr,1);
	raw();
	curs_set(0);
	drawParams(0,1,current,c);
	drawSequencer(0,4,current);
  printLogs(0,22);
	refresh();

	while(c != 17){
		c = getch();

		switch(c){
			case 'a':
				if(!seqToggle)
					selected = 0;
				break;
			case 's':
				if(!seqToggle)
					selected = 1;
				break;
			case 'd':
				if(!seqToggle)
					selected = 2;
				break;
			case 'f':
				if(!seqToggle)
					selected = 3;
				break;
			case 'g':
				if(!seqToggle)
					selected = 4;
				break;
			case 'z':
				if(!seqToggle)
					selected = 5;
				break;
			case 'x':
				if(!seqToggle)
					selected = 6;
				break;
			case 'c':
				if(!seqToggle)
					selected = 7;
				break;
			case 'v':
				if(!seqToggle)
					selected = 8;
				break;
			case 'b':
				if(!seqToggle)
					selected = 9;
				break;
			case 'h':
				if(!seqToggle)
					selected = 10;
				break;
			case 'n':
				if(!seqToggle)
					selected = 11;
				break;
			case 18:
				for(i = 0; i < 32; i++)
					patterns[current->nextPattern].data[i] = random();
        postLogEntry("Pattern randomized.");
				break;
			case 4:
				for(i = 0; i < 32; i++)
					patterns[current->nextPattern].data[i] = 0xd0;
         postLogEntry("Pattern cleared.");
				break;
			case 3:
				pBuf = patterns[current->nextPattern];
        postLogEntry("Pattern copied to buffer.");
				break;
			case 22:
				patterns[current->nextPattern] = pBuf;
         postLogEntry("Pattern pasted from buffer.");
				break;
			case 10:
				if(playToggle){
					seq_stop(&a);
          postLogEntry("Sequencer playback stopped.");
        } else {
          postLogEntry("Sequencer playback started.");
					seq_start(&a);
        }
				playToggle = ~playToggle;
				break;
			case 'o':
				if(patterns[current->nextPattern].length > 1)
					patterns[current->nextPattern].length -= 1;
				if(seqStep == patterns[current->nextPattern].length)
					seqStep--;
        sprintf(strScratch,"Pattern size decreased to %d steps.",patterns[current->nextPattern].length);
        postLogEntry(strScratch);
				break;
			case 'p':				
				if(patterns[current->nextPattern].length < 32)
					patterns[current->nextPattern].length += 1;
        sprintf(strScratch, "Pattern size increased to %d steps.",patterns[current->nextPattern].length);
        postLogEntry(strScratch);
				break;
			case 'O':
				tempNote = patterns[current->nextPattern].data[0];
				for(i=0;i<patterns[current->nextPattern].length-1;i++){
					patterns[current->nextPattern].data[i] = patterns[current->nextPattern].data[i+1];
				}
				patterns[current->nextPattern].data[patterns[current->nextPattern].length - 1] = tempNote;
        postLogEntry("Pattern shifted left.");
				break;
			case 'P':
				tempNote = patterns[current->nextPattern].data[patterns[current->nextPattern].length-1];
				for(i=patterns[current->nextPattern].length-2;i>=0;i--){
					patterns[current->nextPattern].data[i+1] = patterns[current->nextPattern].data[i];
				}
				patterns[current->nextPattern].data[0] = tempNote;				
        postLogEntry("Pattern shifted right.");
				break;
			case ' ':
				if(seqToggle){
					selected = oldSelected;
          postLogEntry("Sequencer mode disabled.");
        } else {
					oldSelected = selected;
					selected = -1;
          postLogEntry("Sequencer mode enabled.");
				}
				seqToggle = ~seqToggle;
				break;
			case 19:
				saveState();
        postLogEntry("State saved.");
				break;
		}
		drawParams(0,1,current,c);
		drawSequencer(0,4,current);
    printLogs(0,22);
		refresh();

	}
	endwin();
}

void drawParams(int x, int y, seq *s, int c){
	temp = 0;
	move(0,0);
  attrset(COLOR_PAIR(3));
  printw("[DX303]---------------------------------------");
  attrset(A_NORMAL);
	if(seqToggle){
		switch(c){
			case LEFT:
				seqStep--;
				if(seqStep == -1)
					seqStep = patterns[s->nextPattern].length - 1;
				break;
			case RIGHT:
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'q':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf);
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '2':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x10;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'w':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x20;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '3':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x30;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'e':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x40;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'r':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x50;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '5':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x60;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 't':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x70;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '6':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x80;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'y':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0x90;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '7':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0xa0;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'u':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0xb0;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case 'i':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0xc0;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '1':
				patterns[s->nextPattern].data[seqStep] = (patterns[s->nextPattern].data[seqStep] & 0xf)+0xd0;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '8':
				temp = patterns[s->nextPattern].data[seqStep];
				temp = (temp & ~0x4) | (~temp & 0x04);
				patterns[s->nextPattern].data[seqStep] = temp;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '9':
				temp = patterns[s->nextPattern].data[seqStep];
				temp = (temp & ~0x8) | (~temp & 0x08);
				patterns[s->nextPattern].data[seqStep] = temp;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
			case '0':
				temp = patterns[s->nextPattern].data[seqStep];
				temp = (temp & ~0x2) | (~temp & 0x02);
				patterns[s->nextPattern].data[seqStep] = temp;
				seqStep++;
				if(seqStep == patterns[s->nextPattern].length)
					seqStep = 0;
				break;
		}
	  if(seqStep >= patterns[current->nextPattern].length)
			seqStep = patterns[s->nextPattern].length - 1;
	} else {

		switch(c) {
			case RIGHT:
				temp = 1;
				break;
			case LEFT:
				temp = -1;
				break;
			case UP:
				temp = 0x10;
				break;
			case DOWN:
				temp= -0x10;
				break;
			case '1':
				s->nextPattern = ((s->nextPattern)&0xf8)+0;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '2':
				s->nextPattern = ((s->nextPattern)&0xf8)+1;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '3':
				s->nextPattern = ((s->nextPattern)&0xf8)+2;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '4':
				s->nextPattern = ((s->nextPattern)&0xf8)+3;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '5':
				s->nextPattern = ((s->nextPattern)&0xf8)+4;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '6':
				s->nextPattern = ((s->nextPattern)&0xf8)+5;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '7':
				s->nextPattern = ((s->nextPattern)&0xf8)+6;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case '8':
				s->nextPattern = ((s->nextPattern)&0xf8)+7;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'q':
				s->nextPattern = ((s->nextPattern)&0x07)+0;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'w':
				s->nextPattern = ((s->nextPattern)&0x07)+8;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'e':
				s->nextPattern = ((s->nextPattern)&0x07)+16;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'r':
				s->nextPattern = ((s->nextPattern)&0x07)+24;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 't':
				s->nextPattern = ((s->nextPattern)&0x07)+32;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'y':
				s->nextPattern = ((s->nextPattern)&0x07)+40;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'u':
				s->nextPattern = ((s->nextPattern)&0x07)+48;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
			case 'i':
				s->nextPattern = ((s->nextPattern)&0x07)+56;
        sprintf(strScratch, "Pattern changed to %d.%d.", (s->nextPattern >> 3) + 1, (s->nextPattern & 7) + 1);
        postLogEntry(strScratch);
				break;
		}
		if(!(s->playing))
			s->pattern = s->nextPattern;
		
	}
	move(y,x);
	HON("FB",0,s->v.fb);
	addch(' ');
	HON("TL1",1,s->v.en1.amp);
	addch(' ');
	HON("ML1",2,s->v.op1Mul);
	addch(' ');
	HON("DC1",3,s->v.en1.decay);
	addch(' ');
	HON("SU1",4,s->v.en1.sustain);
	addch(' ');
	HON("TMP",10,tempo); // General
	move(y+1,x);
	HON("TR",5,s->transpose);
	addch(' ');
	HON("TL2",6,s->v.en2.amp);
	addch(' ');
	HON("ML2",7,s->v.op2Mul);
	addch(' ');
	HON("DC2",8,s->v.en2.decay);
	addch(' ');
	HON("SU2",9,s->v.en2.sustain);
	addch(' ');
	HON("SWG",11,swing); // General
	move(y+2,x);
	attrset(COLOR_PAIR(3));
	if(seqToggle)
		attron(A_REVERSE);
	printw("----------------------------------------------");
	attrset(A_NORMAL);
	tempoCalc(tempo,swing);


}

void drawSequencer(int x, int y, seq *s){
	int i;
	for(i = 0; i < 18; i++){
		move(y+i,x+2);
		printw("                                ");
	}
	attron(COLOR_PAIR(2));
	for(i = 16; i >= 0; i--){
		move(y+16-i,x);
		printw("%s", noteNames[i]);
	}
	attroff(COLOR_PAIR(2));
	for(i = 0; i < patterns[s->nextPattern].length; i++){
		if(seqToggle && seqStep == i)
			attron(COLOR_PAIR(1));
		else
			attron(COLOR_PAIR(2));
		drawSeqRow(x+2+i,y,patterns[s->nextPattern].data[i], !(i&0x3));
		attrset(A_NORMAL);
	}
	if(seqToggle){
		move(y+17,x+seqStep+2);
		printw("^");
	}
}

void drawSeqRow(int x, int y, unsigned char n, char bold){
	int i;
	for(i = 0; i < 17; i++){
		move(y+i,x);
		if(bold)
			addch('|');
		else
			addch('_');
	}
	move(y+13,x);
	addch('+');
	move(y+15,x);

	if(GETOCTUP(n)){
		attron(A_REVERSE);
		move(y+14,x);
		addch(' ');
		attroff(A_REVERSE);
	}
	if(GETOCTDOWN(n)){
		attron(A_REVERSE);
		move(y+15,x);
		addch(' ');
		attroff(A_REVERSE);
	}
	if(GETSLIDE(n)){
		attron(A_REVERSE);
		move(y+16,x);
		addch(' ');
		attroff(A_REVERSE);
	}

	if(GETNOTE(n) < 0xd){
		move(y+12-GETNOTE(n),x);
		attron(A_REVERSE);
		addch(' ');
		attroff(A_REVERSE);
	}
}

void printLogs(int x, int y){
  int i;
  move(y,x);
  attrset(COLOR_PAIR(3));
  printw("[Messages]------------------------------------");
  attrset(A_NORMAL);
  y++;

  for(i = 0; i < LOGBUF; i++){
    if(strlen(logs[i].text)){
      move(y + i, x);
      clrtoeol();
      strftime(strScratch, 255, "[%H:%M:%S]",localtime(&logs[i].time));
      attrset(COLOR_PAIR(3));
      printw(strScratch);
      if(i == LOGBUF - 1)
        attrset(COLOR_PAIR(4));
      else
        attrset(COLOR_PAIR(2));
      move(y + i, x + 11);
      printw(logs[i].text);
    }
  }
}
