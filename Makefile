CC=gcc

fms : main.o fm.o cb.o seq.o interface.o tempo.o fileio.o log.o headless.o
	$(CC) -o dx303 main.o fm.o cb.o seq.o interface.o tempo.o fileio.o log.o headless.o -ljack -lncurses -lm

main.o : main.c headers
	$(CC) -c main.c

fm.o : fm.c headers
	$(CC) -c fm.c

cb.o : cb.c headers
	$(CC) -c cb.c

seq.o : seq.c headers
	$(CC) -c seq.c

interface.o : interface.c headers
	$(CC) -c interface.c

tempo.o : tempo.h headers
	$(CC) -c tempo.c

fileio.o : fileio.c headers
	$(CC) -c fileio.c

log.o : log.c headers
	$(CC) -c log.c

headless.o : headless.c headers
	$(CC) -c headless.c

headers : cb.h common.h fm.h pattern.h seq.h interface.h fileio.h log.h headless.h

clean:
	rm *.o
