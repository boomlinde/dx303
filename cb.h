#ifndef _cb_h_
#define _cb_h_

#include "common.h"

int sound_cb(jack_nframes_t nframes, void *arg);
#endif
