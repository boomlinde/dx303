#ifndef _fm_h_
#define _fm_h_

typedef struct {
	unsigned int phase;
	unsigned int phaseInc;
	int *waveform;
} op;

typedef struct {
	unsigned char amp;
	int ampState;
	unsigned char decay;
	unsigned char sustain;
} env;

typedef struct {
	op op1, op2; // <op1 -> op2
	env en1, en2;
	unsigned char op1Mul;
	unsigned char op2Mul;
	short int op1Last;
	unsigned char fb;
	unsigned char gate;
	int freqIncrement;
	int target;
} voice;

void voice_setFreq(voice *v, int frequency);

void voice_trig(voice *v, int frequency, int target, int speed, unsigned char retrig);

int voice_tick(voice *v);

short int sine[256];

#endif
