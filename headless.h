#ifndef _headless_h_
#define _headless_h_

void headlessInit();
void headlessParser();
void fileParser(const char *name);
#endif
