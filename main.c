#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "common.h"
#include "cb.h"
#include "fm.h"
#include "seq.h"
#include "interface.h"
#include "time.h"
#include "tempo.h"
#include "fileio.h"
#include "log.h"
#include "headless.h"
#include "string.h"

unsigned char headless = 0;

int srate(jack_nframes_t nframes, void *arg){
	int i;
	ratio = ((RATE<<8) / nframes);

	for(i = 0; i<128;i++){
		patterns[i&0x3f].length = 16;
		notes[i] = (int)( pow(2,(i)/12.0) * (440 << 22) / nframes); 
	}
	return 0;
}

void jack_shutdown(){
	exit(1);
}

int main(int argc, char *argv[]){
	int i,j;

  postLogEntry("Welcome!");
  midiDelay = 0;

  for(i = 1; i < argc; i++)
    if(!strcmp("-H", argv[i]))
      headless = 1;
  a.lastNote = 0x0d;
	a.v.en1.amp = 0x20;
	a.v.fb = 0x00;
	a.v.en2.amp = 0x80;
	a.v.en1.decay = 0x40;
	a.v.en2.decay = 0xd0;
	a.v.en1.sustain = 0x10;
	a.v.en2.sustain = 0xc0;
	a.v.op1Mul = 1;
	a.v.op2Mul = 0;
	a.transpose = 0x80;
	last_playState = 0;
	playState = 0;
	if( !( client = jack_client_open("DX303", JackNullOption, NULL)) ){
		fprintf(stderr,"No jack server running\n");
		return 1;
	}
	ratio = ((RATE<<8) / jack_get_sample_rate(client));

	for(i = 0; i<64;i++)
		patterns[i].length = 16;

	for(i = 0; i<64;i++)
		notes[i] = (int)( pow(2,(i)/12.0) * (440 << 22) / jack_get_sample_rate(client)); 

	for(i = 0; i < 256; i++){
		sine[i] = (short int)(32767*sin(i*3.1415/128.0));
	}

	// jack callbacks
	jack_set_process_callback(client, sound_cb, 0);
	jack_set_sample_rate_callback(client, srate, 0);
	jack_on_shutdown(client, jack_shutdown, 0);

	audio_output = jack_port_register(client, "audio_out", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
	midi_output  = jack_port_register(client, "midi_out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);

	if(jack_activate(client)){
		fprintf(stderr, "cannot activate client");
		return 1;
	}

	swing = 0x80;
	tempo = 125;

	srand(time(0));
	for(i = 0; i < 32; i++)
		for(j = 0; j < 64; j++)
			patterns[j].data[i] = 0xd0;
	patterns[0].length = 16;
	loadState();
	tempoCalc(tempo, swing);
  headlessInit();
  fileParser("dx303.conf");
  if(headless)
    headlessParser();
  else
	  startInterface();	
	jack_client_close(client);
	exit(0);
}

