#ifndef _common_h_
#define _common_h_

#include "jack/jack.h"
#include "jack/midiport.h"
#include "pattern.h"
#include "seq.h"
#include "tempo.h"
#include "ncurses.h"


#define RATE 44100

pattern patterns[64];
pattern pBuf;
int notes[128];
int ratio;
seq a;
int tick;
int midiDelay;
char strScratch[1024];

jack_client_t *client;
jack_port_t *audio_output;
jack_port_t *midi_output;
#endif
