#include "common.h"
#include "cb.h"
#include "math.h"
#include "fm.h"
#include "seq.h"
#include "tempo.h"

int tick      = 0;
int tstate    = 0;
int tickCount = 0;

int sound_cb(jack_nframes_t nframes, void *arg){
	int i;
	int s=0;
	jack_default_audio_sample_t *out = jack_port_get_buffer(audio_output, nframes);
	void *midi_buf = jack_port_get_buffer(midi_output, nframes);
	jack_midi_data_t *buffer;
	jack_midi_clear_buffer(midi_buf);

	for(i = 0; i < nframes; i++){
		if( playState && !last_playState ){
			buffer = jack_midi_event_reserve(midi_buf,i,2);
			buffer[0] = 0xfa;
			buffer[1] = 0xf8;
			last_playState = playState;
			tickCount = 0;
		}
		if(tick >= tempoSteps[ ((flip+3) & 3) >> 1 ]){
			if(!(flip&1))
				seq_step(&a,tempoSteps[((flip-1)&3)>>1]);
			else 
				seq_halfStep(&a);
			flip++;
			tick = 0;
		}

		tick++;
		tickCount+=6;
		if(tickCount + midiDelay >= tRate){
			tickCount -= tRate;
			jack_midi_event_reserve(midi_buf,i,1)[0] = 0xf8;	
		}
		if(last_playState && !playState)
			jack_midi_event_reserve(midi_buf,i,1)[0] = 0xfc;
		
		last_playState = playState;

		s = voice_tick(&(a.v));
		if(s > 0x7fff)
			s = 0x7fff;
		else if( s < -0x8000 )
			s = -0x8000;
		out[i] = s/32768.0;
	}
	return 0;
}
