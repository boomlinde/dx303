#include "common.h"
#include "tempo.h"

void tempoCalc(int tempo,int swing){
	if( tempo < 1 )
		tempo = 1;
	tempo = (jack_get_sample_rate(client) << 8) / ( (tempo<<8) / 15 );
	tempoSteps[0] = (tempo*swing+1)>>8;
	tempoSteps[1] = (tempo*(0x100-swing))>>8;
	tRate = (tempoSteps[0] + tempoSteps[1]);
}
