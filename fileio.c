#include "common.h"
#include "fileio.h"
#include "stdio.h"
void saveState(){
	FILE *f;
	f = fopen("state","wb");
	if(!f)
		return;	
	fwrite(&a.v,sizeof(voice), 1, f);
	fputc(tempo, f);
	fputc(swing, f);
	fputc(a.transpose, f);
  fputc(a.pattern, f);
	fwrite(patterns,sizeof(pattern), 64, f);
	fclose(f);
}

void loadState(){
	FILE *f;
	f = fopen("state","rb");
	if(!f)
		return;
	fread(&a.v,sizeof(voice), 1, f);
	tempo = fgetc(f);
	swing = fgetc(f);
	a.transpose = fgetc(f);
  a.pattern = fgetc(f);
  a.nextPattern = a.pattern;
	fread(patterns,sizeof(pattern), 64, f);
	fclose(f);
}
