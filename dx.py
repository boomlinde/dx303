import subprocess


class DX():
    def __init__(self):
        self.dx = subprocess.Popen(["./dx303", "-H"], stdout = subprocess.PIPE, stdin = subprocess.PIPE)

    def cmd(self, s):
        output = []
        line = ""
        s += " ok\n"
        self.dx.stdin.write(s)
        while True:
            line = self.dx.stdout.readline().strip()
            if line == "ok":
                break
            elif line != "":
                output.append(line)
        return output

