#include "common.h"
#include "headless.h"
#include "string.h"

typedef struct {
  char name[20];
  void (*function)();
} word;


word words[256];
int stack[256];
unsigned char sp = 0;
unsigned char wc = 0;
char headlessBuffer[1024];
unsigned char silent = 1;

void execute(const char *name);
void addWord(const char *name, void (*function)());
void push(int n);
int pop();

short unsigned int cs(const char *str){
  short unsigned int sum = 0;
  int i = 0;
  do
    sum += str[i];
  while(str[++i]);
  return sum;
}

void add(){
  push(pop() + pop());
}

void listWords(){
  int i;
  if(silent)
    return;
  for(i = 0; i < wc; i++)
    fprintf(stdout, "%s ",words[i].name);
  fprintf(stdout, "\n");
}

void listPorts(){
  const char **ports;
  int i = 0;

  if(silent)
    return;

  ports = jack_get_ports(client, "", "", 0);

  do
    fprintf(stdout, "%d\t%s\n", cs(ports[i]), ports[i]);
  while(ports[++i]);
}

void connectPorts(){
  const char **ports;
  const char *src;
  const char *dst;
  int src_i, dst_i;
  int i = 0;
  dst_i = pop();
  src_i = pop();
  ports = jack_get_ports(client, "", "", 0);
  do
    if(cs(ports[i]) == src_i)
      src = ports[i];
    else if(cs(ports[i]) == dst_i)
      dst = ports[i];
  while(ports[++i]);
  jack_connect(client, src, dst);
}

void disconnectPorts(){
  const char **ports;
  const char *src;
  const char *dst;
  int src_i, dst_i;
  int i = 0;
  dst_i = pop();
  src_i = pop();
  ports = jack_get_ports(client, "", "", 0);
  do
    if(cs(ports[i]) == src_i)
      src = ports[i];
    else if(cs(ports[i]) == dst_i)
      dst = ports[i];
  while(ports[++i]);
  jack_disconnect(client, src, dst);
}

void play(){
  seq_start(&a);
}

void setPattern(){
  a.nextPattern = pop();
}

void getPattern(){
  push(a.nextPattern);
}

void setTempo(){
  tempo = pop();
  tempoCalc(tempo, swing);
}

void getTempo(){
  push(tempo);
}

void setSwing(){
  swing = pop();
  tempoCalc(tempo,  swing);
}

void getSwing(){
  push(swing);
}

void stop(){
  seq_stop(&a);
}

void subtract(){
  push(pop() - pop());
}

void multiply(){
  push(pop() * pop());
}

void divide(){
  int a = pop();
  push(pop() / a);
}

void lshift(){
  int a = pop();
  push(pop() << a);
}

void rshift(){
  int a = pop();
  push(pop() >> a);
}

void and(){
  push(pop() & pop());
}

void or(){
  push(pop() | pop());
}

void xor(){
  push(pop() ^ pop());
}

void print(){
  if(silent)
    return;
  fprintf(stdout, "%d\n",pop());
}

void quit(){
  fprintf(stdout,"ok\n");
  fflush(stdout);
  jack_client_close(client);
  exit(0);
}

void setMidiDelay(){
  midiDelay = pop();
}

void push(int n){
  stack[sp++] = n;
}

int pop(){
  int ret;
  ret = stack[--sp];
  sp &= 0x1f;
  return ret;
}

void getFeedback(){
  push(a.v.fb);
}

void setFeedback(){
  a.v.fb = pop();
}

void tl1Set(){
  a.v.en1.amp = pop();
}

void tl1Get(){
  push(a.v.en1.amp);
}

void tl2Set(){
  a.v.en2.amp = pop();
}

void tl2Get(){
  push(a.v.en2.amp);
}

void dc1Set(){
  a.v.en1.decay = pop();
}
void dc1Get(){
  push(a.v.en1.decay);
}

void dc2Set(){
  a.v.en2.decay = pop();
}
void dc2Get(){
  push(a.v.en2.decay);
}

void ok(){
  fprintf(stdout,"ok\n");
  fflush(stdout);
}

void su2Set(){
  a.v.en2.sustain = pop();
}
void su2Get(){
  push(a.v.en2.sustain);
}

void su1Set(){
  a.v.en1.sustain = pop();
}
void su1Get(){
  push(a.v.en1.sustain);
}

void ml1Set(){
  a.v.op1Mul = pop();
}
void ml1Get(){
  push(a.v.op1Mul);
}

void ml2Set(){
  a.v.op2Mul = pop();
}
void ml2Get(){
  push(a.v.op2Mul);
}

void setTr(){
  a.transpose = pop();
}

void getTr(){
  push(a.transpose);
}

void headlessInit(){
  addWord("words", listWords);
  addWord("+", add);
  addWord("-", subtract);
  addWord("*", multiply);
  addWord("/", divide);
  addWord("<<", lshift);
  addWord(">>", rshift);
  addWord("&", and);
  addWord("|", or);
  addWord("^", xor);
  addWord(".", print);
  addWord("play", play);
  addWord("stop", stop);
  addWord("ports.disconnect", disconnectPorts);
  addWord("mididelay.set", setMidiDelay);

  addWord("fb.set", setFeedback);
  addWord("fb.get", getFeedback);

  addWord("tl1.set", tl1Set);
  addWord("tl1.get", tl1Get);

  addWord("tl2.set", tl2Set);
  addWord("tl2.get", tl2Get);

  addWord("ml1.set", ml1Set);
  addWord("ml1.get", ml1Get);

  addWord("ml2.set", ml2Set);
  addWord("ml2.get", ml2Get);

  addWord("dc1.set", dc1Set);
  addWord("dc1.get", dc1Get);

  addWord("dc2.set", dc2Set);
  addWord("dc2.get", dc2Get);

  addWord("su1.set", su1Set);
  addWord("su1.get", su1Get);

  addWord("su2.set", su2Set);
  addWord("su2.get", su2Get);

  addWord("tr.set", setTr);
  addWord("tr.get", getTr);

  addWord("tmp.set", setTempo);
  addWord("tmp.get", getTempo);
  addWord("swg.set", setSwing);
  addWord("swg.get", getSwing);
  addWord("ptn.set", setPattern);
  addWord("ptn.get", getPattern);
  addWord("ports.list", listPorts);
  addWord("ports.connect", connectPorts);
  addWord("ok", ok);
  addWord("q", quit);
}


void headlessParser(){
  int d;
  fflush(stdout);
  silent = 0;
  while(scanf("%s", headlessBuffer))
    if(sscanf(headlessBuffer,"%d", &d))
      push(d);
    else
      execute(headlessBuffer);
}

void fileParser(const char *name){
  int d;
  FILE *f;
  silent = 1;
  if(!(f = fopen(name, "r")))
    return;
  while(fscanf(f, "%s", headlessBuffer) != EOF)
    if(sscanf(headlessBuffer,"%d", &d))
      push(d);
    else
      execute(headlessBuffer);
  fclose(f);
}


void execute(const char *name){
  int i;
  for(i = 0; i < wc; i++)
    if(!strcmp(name, words[i].name)){
      words[i].function();
      if(silent)
        return;
      return;
    }
}

void addWord(const char *name, void (*function)()){
  strcpy(words[wc].name, name);
  words[wc++].function = function;
}
